let express = require("express");

const PORT = 8080;

let app = express();

let users = [];

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/home", (req, res) => {
	res.send(`Welcome to the Homepage`);
});

app.post("/signup", (req, res)=>{
	if(req.body.username !== "" && req.body.password !== ""){
		//console.log(typeof req.body.password);
		//console.log(req.body);
		users.push(req.body);
		//console.log(users);
		res.send(`User ${req.body.username} successfully registered!`);
	} else {
		res.send(`Please input both username and password`);
	}
});

app.get("/users", (req, res) => {
	// res.send(
	// 	users.forEach((element)=>{
	// 	`username ${element.username}|| password ${element.password}`;
	// })
	// );
	//console.log(users);
	res.send(users);
	
});


app.listen(PORT, ()=>{
	console.log(`Server is running in port: ${PORT}`);
});
